#include <stdio.h>

#include "mask.h"



static int mask_owner, mask_group, mask_other;



int main() {
	
	char p;
	
	printf("Permiso lectura para owner? [s/n]:\n ");
	
	scanf("%c",&p);
	
	getchar();
	
	setPermiso(&mask_owner,'r',getRespuesta(p));
	
	printf("%i",mask_owner);


	
	printf("Permiso escritura para owner? [s/n]:\n ");
	
	scanf("%c",&p);
	
	getchar();
	
	setPermiso(&mask_owner,'w',getRespuesta(p));
	
	printf("%i",mask_owner);

	
	
	printf("Permiso ejecucion para owner? [s/n]:\n ");
	
	scanf("%c",&p);
	
	getchar();
	
	setPermiso(&mask_owner,'x',getRespuesta(p));
	
	printf("%i",mask_owner);


	
	printf("Permiso lectura para group? [s/n]:\n ");
	
	scanf("%c",&p);
	
	getchar();
	
	setPermiso(&mask_group,'r',getRespuesta(p));
	
	printf("%i",mask_group);


	
	printf("Permiso escritura para group? [s/n]:\n ");
	
	scanf("%c",&p);
	
	getchar();
	
	setPermiso(&mask_group,'w',getRespuesta(p));
	
	printf("%i",mask_group);


	
	printf("Permiso ejecucion para group? [s/n]:\n ");
	
	scanf("%c",&p);
	
	getchar();
	
	setPermiso(&mask_group, 'x',getRespuesta(p));
	
	printf("%i",mask_group);


	
	printf("Permiso lectura para other? [s/n]:\n ");
	
	scanf("%c",&p);
	
	getchar();
	
	setPermiso(&mask_other, 'r',getRespuesta(p));
	
	printf("%i",mask_other);


	
	printf("Permiso escritura para other? [s/n]:\n ");
	
	scanf("%c",&p);
	
	getchar();
	
	setPermiso(&mask_other, 'w',getRespuesta(p));
	
	printf("%i",mask_other);


	
	printf("Permiso ejecucion para other? [s/n]:\n ");
	
	scanf("%c",&p);
	
	getchar();
	
	setPermiso(&mask_other,'x',getRespuesta(p));
	
	printf("%i",mask_other);


	
	printf("Permiso: %i %i %i\n",mask_owner,mask_group,mask_other);

}
