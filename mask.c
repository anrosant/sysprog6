#include "mask.h"

#define SET 1

#define UNSET 0



void setPermiso(int *mascara, char permiso, int set){
	
	switch(permiso){
		
		case 'r':
			
			*mascara= *mascara | (set<<2);
			
			break;
		
		case 'w':
			
			*mascara= *mascara | (set<<1);
			
			break;
		
		case 'x':
			
			*mascara= *mascara | (set);
			
			break;
	
	}

}



int getRespuesta(char r){
	
	switch(r){
		
		case 's':
			
			return SET;
		
		case 'n':
			
			return UNSET;		
	
	}

}
